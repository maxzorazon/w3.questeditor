//
// gui::settings
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub(in gui) struct Settings {
    pub changed: bool,
    pub rpanel_width: f32,
    path: PathBuf,
}
// ----------------------------------------------------------------------------
use std::fs::File;
use std::io::{BufRead, BufReader, BufWriter, Write};
use std::path::{Path, PathBuf};
// ----------------------------------------------------------------------------
const SETTING_RPANEL_WIDTH: &str = "right panel width";
// ----------------------------------------------------------------------------
impl Settings {
    // ------------------------------------------------------------------------
    pub fn load(&mut self, path: &Path) -> Result<(), String> {
        self.path = PathBuf::from(path);

        if path.exists() {
            debug!("reading settings from {}...", path.display());

            let file_reader = File::open(path)
                .map(BufReader::new)
                .map_err(|err| format!("settings: error reading settings file: {}", err))?;

            for (i, line) in file_reader.lines().enumerate() {
                let line =
                    line.map_err(|e| format!("settings: failed to read line {}: {}", i, e))?;
                let keyval = line.split('=').collect::<Vec<_>>();
                if keyval.len() != 2 {
                    warn!("settings: could not parse \"{}\". skipping line...", line);
                }

                match keyval[0].trim().to_lowercase().as_str() {
                    SETTING_RPANEL_WIDTH => {
                        let width = keyval[1].trim().parse::<u16>().map_err(|e| {
                            format!(
                                "settings: failed to parse numerical value in line {}: {}",
                                i, e
                            )
                        })?;
                        self.rpanel_width = 320.max(width).min(500) as f32;
                    }
                    unknown => warn!(
                        "settings: found unknown setting in line {}: {}. ignoring...",
                        i + 1,
                        unknown
                    ),
                }
            }
        }

        Ok(())
    }
    // ------------------------------------------------------------------------
    pub fn save(&mut self) -> Result<(), String> {
        info!("saving settings to [{}]...", self.path.display());

        let mut file_writer = File::create(&self.path)
            .map(BufWriter::new)
            .map_err(|err| {
                format!(
                    "settings writer: couldn't create {}: {}",
                    self.path.display(),
                    err
                )
            })?;

        let setting = format!("{} = {}", SETTING_RPANEL_WIDTH, self.rpanel_width as u16);

        file_writer
            .write(setting.as_bytes())
            .map_err(|e| format!("settings writer: could not write setting: {}", e))?;

        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl Default for Settings {
    // ------------------------------------------------------------------------
    fn default() -> Self {
        Settings {
            changed: false,
            rpanel_width: 320.0,
            path: PathBuf::from("settings.ini"),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
