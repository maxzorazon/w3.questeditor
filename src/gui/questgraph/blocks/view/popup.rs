//
// blocks::view::popup
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use imgui::Ui;
use imgui_controls::popup::PopupView;

// actions
use gui::{Action, PopupAction};

// state

// misc
use imgui_controls::input::Field;

use super::SocketNameRequest;
// ----------------------------------------------------------------------------
impl PopupView<Action> for SocketNameRequest {
    // ------------------------------------------------------------------------
    fn title(&self) -> &str {
        if self.in_socket {
            "Add new in-socket"
        } else {
            "Add new out-socket"
        }
    }
    // ------------------------------------------------------------------------
    fn size(&self) -> (f32, f32) {
        (250.0, 110.0)
    }
    // ------------------------------------------------------------------------
    fn draw<'ui>(&mut self, ui: &Ui<'ui>) -> Option<Action> {
        let mut result = None;

        ui.with_region_height(im_str!("##new_socket"), 2.25, || {
            ui.text_wrapped(im_str!(
                "Enter a (case-sensitive!) name"
            ));
            ui.spacing();
            ui.spacing();
            if let Some(new_value) = self.socketname.draw(ui) {
                result = Some(PopupAction::UpdateField(
                    "socketname".into(),
                    new_value.into(),
                ).into());
            }
            ui.spacing();
        });

        result
    }
    // ------------------------------------------------------------------------
    fn valid(&self) -> bool {
        self.socketname.valid()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
