//
// blocks::popup
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub(in super) struct SocketNameRequest {
    pub(super) id: BlockId,
    pub(super) in_socket: bool,
    pub(super) socketname: input::TextField,
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use imgui_controls::input;
use imgui_controls::input::validator;
use imgui_controls::input::Field;
use imgui_controls::popup::{PopupContent, PopupControl};

// actions
use gui::{Action, PopupAction};
use super::BlockAction;

// state

// misc
use super::{BlockId, InSocketId, OutSocketId};

// ----------------------------------------------------------------------------
impl SocketNameRequest {
    pub fn new(id: BlockId, in_socket: bool) -> SocketNameRequest {
        SocketNameRequest {
            id,
            in_socket,
            socketname: input::TextField::new::<_, &str>("##socketname", None)
                // .set_label_width(properties::LABEL_WIDTH)
                .set_validators(vec![
                    validator::is_nonempty(),
                    validator::min_length(3),
                    validator::max_length(64),
                    validator::chars("^[0-9_a-zA-Z]*$", "[a-z_0-9]"),
                ]),
        }
    }
}
// ----------------------------------------------------------------------------
impl PopupContent<PopupAction, Action> for SocketNameRequest {}
// ----------------------------------------------------------------------------
impl PopupControl<PopupAction, Action> for SocketNameRequest {
    // ------------------------------------------------------------------------
    fn process_action(&mut self, action: PopupAction) -> Option<Action> {
        match action {
            PopupAction::UpdateField(id, value) => match id.as_str() {
                "socketname" => {
                    if self.socketname.validate().valid() {
                        self.socketname.set_value((&value).into()).ok();
                    }
                }
                unknown => warn!(
                    "update action for unknown popup field [{}]: {}",
                    unknown, value
                ),
            }
        }
        None
    }
    // ------------------------------------------------------------------------
    fn validate(&mut self) {
        self.socketname.validate();
    }
    // ------------------------------------------------------------------------
    fn on_ok(&self) -> (bool, Option<Action>) {
        if self.socketname.valid() {
            if let Ok(name) = self.socketname.value().as_str() {
                let action = if self.in_socket {
                    BlockAction::AddInSocket(self.id.clone(), InSocketId::from(name))
                } else {
                    BlockAction::AddOutSocket(self.id.clone(), OutSocketId::from(name))
                };
                (true, Some(action.into()))
            } else {
                (false, None)
            }
        } else {
            (false, None)
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// debug trait impl
// ----------------------------------------------------------------------------
use std::fmt;

impl fmt::Debug for SocketNameRequest {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "SocketNameRequest")
    }
}
// ----------------------------------------------------------------------------
