//
// gui::view::menu
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub(in gui) fn render<'a>(ui: &Ui<'a>, state: &State) -> Option<MenuSelection> {
    let mut result = None;
    ui.main_menu_bar(|| {
        ui.menu(im_str!("Project")).build(|| {
            if ui.menu_item(im_str!("New")).build() {
                result = Some(MenuSelection::NewProject);
            }
            let mut auto_reload = state.data.auto_reload;
            if ui
                .menu_item(im_str!("Auto reload on change"))
                .selected(&mut auto_reload)
                .build()
            {
                result = Some(MenuSelection::SetAutoReload(auto_reload));
            }
            ui.separator();
            if ui.menu_item(im_str!("Load")).build() {
                result = Some(MenuSelection::LoadProject);
            }
            if ui
                .menu_item(im_str!("Reload"))
                .enabled(!state.data.is_new())
                .build()
            {
                result = Some(MenuSelection::ReloadProject);
            }
            if ui
                .menu_item(im_str!("Save"))
                .enabled(!state.data.is_new() && state.data.is_available())
                .build()
            {
                result = Some(MenuSelection::SaveProject);
            }
            if ui
                .menu_item(im_str!("Save in..."))
                .enabled(state.data.is_available())
                .build()
            {
                result = Some(MenuSelection::SaveProjectAtLocation);
            }
            ui.separator();
            if ui
                .menu_item(im_str!("Close"))
                .enabled(state.data.is_available())
                .build()
            {
                result = Some(MenuSelection::CloseProject);
            }
            ui.separator();
            if ui.menu_item(im_str!("Quit editor")).build() {
                result = Some(MenuSelection::Quit);
            }
        });

        // mode specific main menus
        if let Some(ref def) = state.data.definition {
            if let EditMode::QuestGraph = state.mode {
                questgraph_main_menu(ui, state.data.graph.as_ref(), def, &mut result);
            }
        }

        ui.menu(im_str!("|")).enabled(false).build(|| {});
        if ui.menu_item(im_str!("Settings")).build() {
            result = Some(MenuSelection::ShowSettings);
        }
        ui.menu(im_str!("|")).enabled(false).build(|| {});

        ui.menu(im_str!("Help")).build(|| {
            if ui.menu_item(im_str!("Documentation")).build() {
                result = Some(MenuSelection::ShowHelp);
            }
            let mut context_help = state.help.context_help();
            if ui
                .menu_item(im_str!("Context Help"))
                .enabled(!state.help.is_empty())
                .selected(&mut context_help)
                .build()
            {
                result = Some(MenuSelection::ShowContextHelp(context_help));
            }
            ui.separator();
            if ui.menu_item(im_str!("About")).build() {
                result = Some(MenuSelection::ShowAbout);
            }
        });
        // #[cfg(debug_assertions)]
        // debug::show_debug_menu(ui, state);
    });
    result
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use imgui::Ui;

use super::MenuSelection;

use super::{EditMode, State};

use super::questgraph;
// ----------------------------------------------------------------------------
#[inline]
fn questgraph_main_menu<'ui>(
    ui: &Ui<'ui>,
    graph: Option<&questgraph::GraphState>,
    def: &::model::QuestDefinition,
    result: &mut Option<MenuSelection>,
) {
    if let (Some(graph), Some(structure)) = (graph, def.structure()) {
        if let Some(action) = questgraph::view::menu::show_main_menus(ui, graph, structure) {
            *result = Some(MenuSelection::QuestGraph(questgraph::Action::Menu(action)));
        }
    }
}
// ----------------------------------------------------------------------------
