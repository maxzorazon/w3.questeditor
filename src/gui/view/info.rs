//
// gui::view::info
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub(in gui) fn render<'a>(ui: &Ui<'a>, area: &UiArea, state: &QuestData) {
    // let padding = ui.imgui().style().window_padding;
    // ui.with_style_var(imgui::StyleVar::WindowPadding((padding.x, 0.0).into()), || {
    ui.window(im_str!("generic_info"))
        .show_borders(false)
        .title_bar(false)
        .menu_bar(false)
        .movable(false)
        .resizable(false)
        .collapsible(false)
        .position(area.pos, imgui::ImGuiCond::Always)
        .size_constraints((area.size.0, 0.0), area.size)
        .always_auto_resize(true)
        .build(|| {
            ui.spacing();
            ui.spacing();
            ui.text(im_str!("dlc id: "));
            ui.same_line(0.0);
            ui.text(&state.name);
            if state.changed() {
                ui.same_line(0.0);
                ui.text(im_str!("*"));
            }
            ui.spacing();

            // if ui.collapsing_header(im_str!("settings")).build() {
            //     ui.text(im_str!(""));
            // };
        });
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use imgui;
use imgui::Ui;

use super::QuestData;
use super::UiArea;
// ----------------------------------------------------------------------------
